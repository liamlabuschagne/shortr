<?php
require 'vendor/autoload.php';
header("Content-Type: application/json");

use liaml\shortr\Models\Redirect;

$uri = $_SERVER['REQUEST_URI'];

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    if (preg_match("#^\/api\/redirects\/\w+$#", $uri)) {
        $_id = explode("/", $uri)[3];
        $redirect = new Redirect();
        $redirect->read($_id);
    }
} else {
    if (preg_match("#^\/api\/redirects$#", $uri)) {
        $redirect = new Redirect($_POST['_id'], $_POST['url']);
        $redirect->create();
    }
}
