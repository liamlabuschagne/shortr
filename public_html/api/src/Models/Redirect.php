<?php
namespace liaml\shortr\Models;

use liaml\shortr\Core\Database;

class Redirect
{
    private $client = null;

    private $_id = "";
    private $url = "";

    public function __construct($_id = "", $url = "")
    {
        $this->client = (new Database())->getClient();

        $this->_id = $_id;
        $this->url = $url;
    }

    public function create()
    {
        $collection = $this->client->shortr->redirect;

        $pasteToInsert = ['_id' => $this->getToken(6), 'url' => $this->url];
        if ($this->_id != "") {
            $_id = $this->_id;
            try {
                $_id = new \MongoDB\BSON\ObjectId($_id);
            } catch (\Exception$e) {}

            $cursor = $collection->find(
                [
                    '_id' => $_id,
                ]
            );

            foreach ($cursor as $restaurant) {
                echo json_encode(["error" => "Custom url already taken."]);
                return;
            };
            $pasteToInsert = ['_id' => $this->_id, 'url' => $this->url];
        }

        $result = $collection->insertOne($pasteToInsert);

        echo json_encode(["_id" => (string) $result->getInsertedId()]);
    }

    public function read($_id)
    {
        $collection = $this->client->shortr->redirect;

        try {
            $_id = new \MongoDB\BSON\ObjectId($_id);
        } catch (\Exception$e) {}

        $result = $collection->findOne(["_id" => $_id]);

        echo json_encode($result);
    }

    private function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) {
            return $min;
        }
        // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    private function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }

        return $token;
    }
}
