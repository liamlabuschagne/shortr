<?php
namespace liaml\shortr\Core;

class Database
{
    private $client;

    public function __construct()
    {
        $this->client = new \MongoDB\Client("mongodb://raspi-server:27017");
    }

    public function getClient()
    {
        return $this->client;
    }
}
