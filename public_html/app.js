document.querySelector("form").addEventListener("submit", (e) => {
    e.preventDefault();
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/api/redirects");
    xhr.onloadend = () => {

        let response = JSON.parse(xhr.responseText);
        if (response.error) {
            alert(response.error);
            return;
        }

        document.querySelector("form").remove();
        document.querySelector("#redirect").style.display = "block";

        document.querySelector("a").href = "/" + response._id;
        document.querySelector("a").textContent = "https://shortr.ml/" + response._id;
    }
    xhr.send(new FormData(e.target));
});

let urlSplit = window.location.pathname.split("/");

if (urlSplit[1]) {
    let id = urlSplit[1];
    let xhr = new XMLHttpRequest();
    xhr.open("GET", "/api/redirects/" + id);
    xhr.onloadend = () => {
        let redirect = JSON.parse(xhr.responseText);
        window.location.href = redirect.url;
    }
    xhr.send()
}

let copyBtn = document.querySelector('#copy');
copyBtn.addEventListener('click', function (event) {
    let copyA = document.querySelector('a');
    let textarea = document.createElement("textarea");
    textarea.textContent = copyA.textContent;
    document.body.appendChild(textarea);
    textarea.focus();
    textarea.select();
    try {
        let successful = document.execCommand('copy');
        if (successful) {
            copied.className = "fade";
            setTimeout(() => { copied.className = ""; }, 2000);
        }
    } catch (err) {
        alert('Unable to copy');
    }
    setTimeout(function () {
        textarea.selectionStart = 0;
        textarea.selectionEnd = 0;
    }, 0);
    textarea.remove();
});